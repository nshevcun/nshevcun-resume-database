import React from 'react';

class InputField extends React.Component {
    render() {
        return(
            <div className = "inputField">
                <input 
                className = 'input'
                type = {this.props.type}
                // This will inherit the parent type
                placeholder = {this.props.placeholder}
                value = {this.props.value}
                onChange = { (elem) => this.props.onChange (elem.target.value)}
                />
            </div>
        );
    }
}

export default InputField;