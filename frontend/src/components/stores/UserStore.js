import { extendObservable } from 'mobx';

/**
 * Here, the user info will be stored.
 */

class UserStore {
    constructor() {
        extendObservable(this, {
            loading: true,
            isLoggedIn: false,
            username: ''
        })
    }
}

export default new UserStore();