const bcrypt = require('bcrypt');

class myRouter {

    condtructor(app, db) {
        this.login(app, db);
        this.logout(app, db);
        this.isLoggedIn(app, db);
    }

    login(app, db) {

        // Use the route defined in front end
        app.post('/login', (req, res) => {
            let username = req.body.username;
            let password = req.body.password;

            username = username.toLowerCase();

            if (username.length > 12 || password.length > 12) {
                res.json({
                    success: false,
                    msg: 'Something went wrong, please try again'
                })
                return;
            }

            let cols = [username];
            db.query('SELECT * FROM credentials WHERE username = ? LIMIT 1', cols, (err, data, fields) => {

                if (err) {
                    res.json({
                        success: false,
                        msg: 'Something went wrong, please try again'
                    })
                    return;
                }

                // Found a user with the username specified
                if (data && data.length === 1) {
                    bcrypt.compare(password, data[0].password, (bcryptErr, verified) => {

                        if (verified) {
                            req.session.userId = data[0].id;

                            res.json({
                                success: true,
                                username: data[0].username
                            })

                            return;
                        }

                        else {
                            res.json({
                                success: false,
                                msg: 'Invalid credentials'
                            })
                        }
                    });
                } else {
                    res.json({
                        success: false,
                        msg: 'No such user! Try again, please...'
                    })
                }
            });
        });

    }

    logout(app, db) {

        app.post('/logout', (req, res) => {
            if (req.session.userId) {
                req.session.destroy();
                res.json({
                    success: true
                })

                return true;
            } else {
                res.json({
                    success: false
                })

                return false;
            }
        });

    }

    isLoggedIn(app, db) {

        app.post('/isLoggedIn', (req, res) => {

            if (req.session.userId) {

                let cols = [req.session.uerId];
                db.query('SELECT * FROM credentials WHERE userId = ? LIMIT 1', cols, (err, data, fields) => {

                    if (data && data.length === 1) {
                        res.json({
                            success: true,
                            username: data[0].username
                        })

                        return true;
                    } else {
                        res.json({
                            success: false
                        })
                    }
                });
            }

            else {
                res.json({
                    success: false
                })
            }
        })
    }

}

module.exports = myRouter;