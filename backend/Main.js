const express = require('express');
const app = express();
const path = require('path');
const mysql = require('mysql');
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);
const myRouter = require('./myRouter');

app.use(express.static(path.join(__dirname, 'build')));
app.use(express.json());

// Database
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '', // Deliberately left empty
    database: 'resume'
});

db.connect(function(err){
    if (err) {
        console.log('DB error');
        throw err;
        return false;
    }  
});

const sessionStore = new MySQLStore({
    expiration: (3600000),
    endConnectionOnClose: false
}, db);

// Random values
app.use(session({
    key: '6578597597474754fgdry546u5rytf',
    secret: 'guk75itfutjt657i6toguggy',
    store: sessionStore,
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: (3600000),
        httpOnly: false
    }
}));

console.log("Still working...");

new myRouter(app, db);

app.get('/', function(req, res) {
    res.sendFile(path.join(_dirname, 'build', 'index.html'));
});

app.listen(3000);